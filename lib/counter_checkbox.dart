import 'package:flutter/material.dart';


class CheckboxCounter extends StatelessWidget {
  CheckboxCounter({this.counter, this.checkboxValue, this.onCheckboxChanged});
  
  final int counter;
  final bool checkboxValue;
  final CheckboxChanged onCheckboxChanged;

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Row(
        children: <Widget>[
          Checkbox(
            value: checkboxValue,
            onChanged: (bool newValue) {
              onCheckboxChanged(newValue);
            }
          ),
          Text('Counter checkbox: $counter'),
        ],
      ),
      color: const Color(0xFFFFA202),
      constraints: BoxConstraints(minHeight: 150.0)
    );
  }
}

typedef CheckboxChanged = void Function(bool newValue);