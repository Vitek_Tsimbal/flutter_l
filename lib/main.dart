import 'package:flutter/material.dart';
import 'counter_checkbox.dart';
import 'counter_widget_button.dart';

void main() => runApp(MaterialApp(
    title: 'Navigation Basics',
    home: FirstRoute()
  ));

class FirstRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<FirstRoute> {
  int counter = 0;
  bool checkboxValue = false;

  Function increaseCounter () {
    if (checkboxValue) {
      setState(() {
        counter++;
      });
    }
  }

  Function onCheckboxChanged (bool newValue) {
    setState(() {
      if (checkboxValue) {
        checkboxValue = false;
      } else {
        checkboxValue = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('First page'),
          backgroundColor: Colors.orangeAccent,
        ),
        body: new Container(
          child: new Column(
            children: <Widget>[
              new Container(
                child: new Row(
                  children: <Widget>[
                    CheckboxCounter(
                      counter: counter,
                      checkboxValue: checkboxValue,
                      onCheckboxChanged: onCheckboxChanged
                    )
                  ],
                ),
                color: const Color(0xFFFFA202),
                constraints: BoxConstraints(minHeight: 150.0)
              ),
              new Container(
                child: new Column (
                  children: <Widget>[
                    new Container(
                      child: new IncreaseCounterButton(
                          increaseCounter: increaseCounter,
                          buttonText: 'Increase counter'
                        )
                    ),
                    new Container(
                      child: new IncreaseCounterButton(
                        increaseCounter: () {
                          setState(() {
                            counter = 0;
                          });
                        },
                        buttonText: 'Drop counter'
                      )
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center
                ),
                color: const Color(0xFFAAE306),
                constraints: BoxConstraints(minHeight: 250.0, maxHeight: 400.0),
              ),
              new Container (
                child: new Row (
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        RaisedButton (
                          child: Text('Go To Second Route'),
                          onPressed: () {
                            print('Go to second page');
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => SecondRoute()),
                            );
                          },
                        )
                      ]
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center
                ),
              )
            ],
          ),
          color: const Color(0xFFFFE306),
        )
      )
    );
  }
}


class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}