import 'package:flutter/material.dart';


class IncreaseCounterButton extends StatelessWidget {
  IncreaseCounterButton({this.increaseCounter, this.buttonText});

  final IncreaseCounter increaseCounter;
  final buttonText;

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Row(
        children: <Widget>[
          RaisedButton(
            padding: const EdgeInsets.symmetric(horizontal: 100.0),
            elevation: 4.0,
            splashColor: Colors.blueGrey,
            onPressed: () {
              increaseCounter();
            },
            color: Theme.of(context).accentColor,
            child: Text('$buttonText')
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceAround,
      ),
    );
  }
}

typedef IncreaseCounter = void Function();